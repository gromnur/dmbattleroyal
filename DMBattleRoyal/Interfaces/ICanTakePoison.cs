﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMBattleRoyal.Interfaces
{
    interface ICanTakePoison
    {
        void TakePoisonDamage();

        void AddPoisonStack(int _dammage);
    }
}
