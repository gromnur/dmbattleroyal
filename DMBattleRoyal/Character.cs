﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Character
    {
        public string Name { get; set; }
        protected int Attack { get; set; }
        protected int Defense { get; set; }
        protected double AttackSpeed { get; set; }
        protected int Damages { get; set; }
        protected int MaxLife { get; set; }
        protected double PowerSpeed { get; set; }
        public int CurrentLife { get; set; }
        public bool CanAttack { get; set; }
        public bool HolyDamages { get; set; }
        public bool Cursed { get; set; }
        public int RandomSeed { get; set; }
        protected Random random;

        public FightManager fightManager;

        protected int attackDelay;
        protected int spellDelay;

        public DateTime nextAttack;
        public DateTime nextSpell;
        public bool IsVisible;
        public Character(string name, int attack, int defense, double attackSpeed, int damages, int maxLife, double powerSpeed, bool holyDamages = false, bool cursed = false)
        {
            Name = name;
            Attack = attack;
            Defense = defense;
            AttackSpeed = attackSpeed;
            Damages = damages;
            MaxLife = maxLife;
            PowerSpeed = powerSpeed;
            HolyDamages = holyDamages;
            Cursed = cursed;
            RandomSeed = NameToInt() + (int)DateTime.Now.Ticks;
            this.random = new Random(RandomSeed);
            
            Reset();
        }

        public virtual void SetFightManager(FightManager fightManager)
        {
            this.fightManager = fightManager;
        }

        public virtual void Reset()
        {
            CurrentLife = MaxLife;
            IsVisible = true;
            spellDelay = 0;
            attackDelay = 0;
        }

        protected int RoundToInt(float value)
        {
            return (int)Math.Round(value);
        }

        public virtual int RollDice()
        {
            return random.Next(1, 101);
        }

        public int NameToInt()
        {
            int result = 0;
            foreach (char c in Name)
            {
                result += c;
            }
            return result;
        }

        public virtual void StartFight()
        {
           while (CurrentLife > 0 && fightManager.charactersList.Count - 1 > fightManager.deadCharacter.Count)
           {

                if (DateTime.Now > nextAttack)
                {
                    SelectTargetAndAttack();
                }

                if (DateTime.Now > nextSpell)
                {
                    ActiveSpell();
                }
           }
        }

        protected virtual void MakeAnAttack(Character target)
        {
            target.Defend(Attack + RollDice(), Damages, this);
            AttackCooldown();
        }

        protected virtual void AddAttackDelay(int dammageTaken)
        {
            nextAttack.AddMilliseconds(dammageTaken);
        }

        public virtual void ActiveSpell()
        {
            SpellCooldown();
        }

        public virtual void SpellCooldown()
        {
            nextSpell = DateTime.Now.AddMilliseconds(Convert.ToInt32(1000 / PowerSpeed - RollDice()));
        }

        protected virtual void AttackCooldown()
        {
            nextAttack = DateTime.Now.AddMilliseconds(Convert.ToInt32(1000 / AttackSpeed - RollDice()));
        }

        public virtual int Defend(int _attackValue, int _damage, Character _attacker)
        {
            //On calcule la marge d'attaque
            //en soustrayant le jet de defense du personnage qui defend au jet d'attaque reçu
            int AttaqueMargin = _attackValue - (Defense + RollDice());
            int finalDamages = 0;
            //Si la marge d'attaque est supérieure à 0
            if (AttaqueMargin > 0 && CurrentLife > 0)
            {
                //on calcule les dégâts finaux
                finalDamages = (int)(AttaqueMargin * _damage / 100f);
                if (_attacker.HolyDamages && Cursed)
                {
                    return TakeDamages(finalDamages*2);
                }
                else
                {
                    return TakeDamages(finalDamages);
                }
            }
            return finalDamages;
        }

        public virtual int TakeDamages(int _damages)
        {
           
            CurrentLife -= _damages;
            AddAttackDelay(_damages);
            MyLog("Subit " + _damages + ", il lui reste " + CurrentLife);
            if (CurrentLife <= 0)
            {
                ImDead();
            }
            return _damages;
        }

        public virtual void ImDead()
        {
            fightManager.RemoveCharacter(this);
        }

        //selectionner une cible valide
        public virtual void SelectTargetAndAttack()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> validTarget = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0 && currentCharacter.IsVisible)
                {
                    //on l'ajoute à la liste des cible valide
                    validTarget.Add(currentCharacter);
                }
            }                       

            if (validTarget.Count > 0)
            {
                //on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque
                Character target = validTarget[random.Next(0, validTarget.Count)];
                MakeAnAttack(target);
            }
        }

        public void MyLog(string text)
        {
            TimeSpan span = DateTime.Now - fightManager.startTime;
            Console.WriteLine(span + " - " + Name + " : " + text);
        }

        public int getMaxLife()
        {
            return MaxLife;
        }

        public virtual Character Clone()
        {
            return new Character(Name, Attack, Defense, AttackSpeed, Damages, MaxLife, PowerSpeed, HolyDamages, Cursed);
        }
    }
}
