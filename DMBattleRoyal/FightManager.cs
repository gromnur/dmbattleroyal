﻿using DMBattleRoyal;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class FightManager
    {
        public ConcurrentBag<Character> charactersList = new ConcurrentBag<Character>();
        public List<Character> deadCharacter = new List<Character>();
        public DateTime startTime;
        public Boolean IsResultAviable;
        public Result Result;
        public int nbJoueur;
        public DelSomeOneIsDead SomeOneIsDead;

        public delegate void DelSomeOneIsDead(Character c);
        public FightManager(ConcurrentBag<Character> charactersList)
        {
            IsResultAviable = false;
            this.charactersList = charactersList;
            nbJoueur = charactersList.Count;
            foreach (Character character in charactersList)
            {
                character.SetFightManager(this);
            }
        }

        public void RemoveCharacter(Character c)
        {
            if (SomeOneIsDead != null)
            {
                SomeOneIsDead(c);
            }
            MyLog(c.Name + " est mort.");
            deadCharacter.Add(c);
        }

        public void StartCombat()
        {
            startTime = DateTime.Now;
            //faire en sorte que les personnages ne soient pas blessé avant le début du combat

            List<Task> tasks = new List<Task>();

            foreach (Character personnage in charactersList)
            {
                personnage.Reset();
            }

            foreach (Character personnage in charactersList)
            {

                tasks.Add(Task.Run(() => personnage.StartFight()));
            }

            while (deadCharacter.Count < nbJoueur - 1)
            {
                tasks.RemoveAt(Task.WaitAny(tasks.ToArray()));
            }

            
            Task.WaitAll(tasks.ToArray());

            ManageVictory();
        }

        public void ManageVictory()
        {

            Dictionary<Character, int> score = new Dictionary<Character, int>();

            int counter = 0;

            foreach (Character c in deadCharacter)
            {
                if (c is Illusionnistse)
                {
                    if (!((Illusionnistse)c).IsClone){
                        score.Add(c, counter);
                        counter++;
                    }
                } else
                {
                    score.Add(c, counter);
                    counter++;
                }
            }

            if (charactersList.Count - deadCharacter.Count == 1)
            {
                // Recupere le character qui n'est pas mort
                var query =
                    from c in charactersList
                    where !(from o in deadCharacter
                            select o).Contains(c)
                    select c;
                foreach (var c in query) score.Add(c, counter);
            }

            Result = new Result(score, DateTime.Now - startTime);

            IsResultAviable = true;
        }


        public void MyLog(string text)
        {
            Console.WriteLine(text);
        }
        
    }
}
