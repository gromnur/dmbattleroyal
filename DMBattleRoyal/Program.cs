﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Program
    {
        static void Main(string[] args)
        {
            Character j1 = new Alchimiste("A");
            Character j2 = new Berseker("B");
            Character j3 = new Guerrier("C");
            Character j4 = new Illusionnistse("D");
            Character j5 = new Magicien("E");
            Character j6 = new Paladin("F");
            Character j7 = new Pretre("G");
            Character j8 = new Robot("H");
            Character j9 = new Vampire("I");
            Character j10 = new Zombie("J");
            Character j11 = new Assassin("K");
            Character j12 = new Necromencien("L");

            // Evite un bug avec l'illusioniste qui ajoute des joueurs
            ConcurrentBag<Character> listeJoueur = new ConcurrentBag<Character>();

            listeJoueur.Add(j1);
            listeJoueur.Add(j2);
            listeJoueur.Add(j3);
            listeJoueur.Add(j4);
            listeJoueur.Add(j5);
            listeJoueur.Add(j6);
            listeJoueur.Add(j7);/*
            listeJoueur.Add(j8);
            listeJoueur.Add(j9);
            listeJoueur.Add(j10);
            listeJoueur.Add(j11);
            listeJoueur.Add(j12);*/

            IEnumerable<KeyValuePair<String, int>> scores = CombatSimultane(listeJoueur, 5);

            // Affiche le resulat
            foreach (KeyValuePair<String, int> item in scores)
            {
                Console.WriteLine(item.Key + " score " + item.Value);
            }
        }
        public static IEnumerable<KeyValuePair<String, int>> CombatSimultane(ConcurrentBag<Character> refCharacters, int nbCombat)
        {
            Dictionary<String, int> listScore = new Dictionary<String, int>();

            List<FightManager> fightManagers = new List<FightManager>();
            List<Task> listTask = new List<Task>();

            for (int i = 0; i < nbCombat; i++)
            {
                // Duplication des caractère
                ConcurrentBag<Character> characters = new ConcurrentBag<Character>();
                foreach (Character c in refCharacters)
                {
                    characters.Add(c.Clone());
                }

                FightManager fm = new FightManager(characters);
                fightManagers.Add(fm);
                listTask.Add(Task.Run(() => fm.StartCombat()));
            }

            Task.WaitAll(listTask.ToArray());

            // Boucle sur les fightmanager
            foreach (FightManager fm in fightManagers)
            {
                // Agrége touts résultat
                foreach (KeyValuePair<Character, int> item in fm.Result.Score)
                {
                    listScore.TryGetValue(item.Key.Name, out int scoreValue);
                    listScore[item.Key.Name] = scoreValue + item.Value;
                }
            }
            // passage en liste pour trier les résultats
            return listScore.ToList().OrderBy(i => i.Value);
        }
    }
}