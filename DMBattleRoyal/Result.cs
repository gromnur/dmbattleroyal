﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DMBattleRoyal
{
    class Result
    {
        public Dictionary<Character, int> Score;
        public TimeSpan FightLength;

        public Result(Dictionary<Character, int> result, TimeSpan fightLength)
        {
            Score = result;
            FightLength = fightLength;
        }

        public override string ToString()
        {
            String toString = "";
            toString += "Durée du combat : " + FightLength + "\n";
            foreach (KeyValuePair<Character,int> item in Score)
            {
                toString += item.Key.Name + " score " + item.Value + "\n";
            }
            return toString;
        }
    }
}
