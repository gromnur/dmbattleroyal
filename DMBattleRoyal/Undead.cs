﻿using System;

namespace DMBattleRoyal
{
    class Undead : Character
    {
        public Undead(string name, int attack, int defense, double attackSpeed, int damages, int maxLife, double powerSpeed, bool cursed = true) : base(name, attack, defense, attackSpeed, damages, maxLife, powerSpeed, cursed:cursed) { }

    }
}
