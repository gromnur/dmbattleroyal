﻿using DMBattleRoyal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class LivingBeing : Character, ICanTakePoison
    {

        public int PoisonStack { get; set; }
        public DateTime nextPoisonTick { get; set; }

        public LivingBeing(string name, int attack, int defense, double attackSpeed, int damages, int maxLife, double powerSpeed, bool holyDamages = false) : base(name, attack, defense, attackSpeed, damages, maxLife, powerSpeed, holyDamages: holyDamages)
        {
            PoisonStack = 0;
        }

        public override void StartFight()
        {
            while (CurrentLife > 0 && fightManager.charactersList.Count - 1 > fightManager.deadCharacter.Count)
            {
                if (DateTime.Now > nextPoisonTick && PoisonStack > 0 && CurrentLife > 0)
                {
                    TakePoisonDamage();
                }

                if (DateTime.Now > nextAttack)
                {
                    SelectTargetAndAttack();
                }

                if (DateTime.Now > nextSpell)
                {
                    base.ActiveSpell();
                }
            }
        }

        public void AddPoisonStack(int _dammage)
        {
            PoisonStack += _dammage;
        }

        public void TakePoisonDamage()
        {
            MyLog("I take poison");
            base.TakeDamages(PoisonStack);
            nextPoisonTick = DateTime.Now.AddMilliseconds(5000);
        }
    }
}
