﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Berseker : LivingBeing
    {
        public double BaseAttackSpeed { set; get; }
        public double BaseDamages { set; get; }
        public double BaseAttack { set; get; }

        public Berseker(string name) : base(name, 50, 50, 1.1, 20, 400, 1)
        {
            BaseAttackSpeed = AttackSpeed;
            BaseDamages = Damages;
            BaseAttack = Attack;
        }

        protected override void AddAttackDelay(int _damages)
        {
            if (CurrentLife >= MaxLife / 2)
            {
                base.AddAttackDelay(_damages);
            }
        }

        public override void ActiveSpell()
        {
            
            Damages = (int)BaseDamages + ((MaxLife - CurrentLife) / 2);
            Attack = (int)BaseAttack + ((MaxLife - CurrentLife) / 2);

            double _percentLife =  (1 - (double) CurrentLife / MaxLife) * 10.0;
            AttackSpeed = BaseAttackSpeed + Convert.ToInt32(_percentLife * 0.3);
            base.ActiveSpell();
        } 
        
        public override Character Clone()
        {
            return new Berseker(Name);
        }
    }
}
