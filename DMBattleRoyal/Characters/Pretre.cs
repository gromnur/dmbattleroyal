﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Pretre : LivingBeing
    {
        public Pretre(string name) : base(name, 60, 145, 1.6, 40, 250, 0.5, true) { }

        public override void ActiveSpell()
        {
            nextAttack = DateTime.Now;
            base.ActiveSpell();
        }

        public override Character Clone()
        {
            return new Pretre(Name);
        }
    }
}
