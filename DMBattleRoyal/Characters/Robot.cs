﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Robot : Character
    {
        public Robot(string name) : base(name, 25, 100, 1.2, 50, 275, 0.5) { }

        public override int RollDice()
        {
            return 50;
        }
        public override void ActiveSpell()
        {
            Attack = Convert.ToInt32(Attack*1.5);
            base.ActiveSpell();
        }

        public override Character Clone()
        {
            return new Robot(Name);
        }
    }
}
