﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Magicien : LivingBeing
    {
        protected List<Character> AttackedTarget;

        public Magicien(string name) : base(name, 75, 125, 1.5, 100, 125, 0.1) {
           
        }

        public override void ActiveSpell()
        {
            // remise à 0 de la liste
            AttackedTarget = new List<Character>();

            // Selectionne les cible
            Character target = SelectSpellPrimaryTarget();

            AttackedTarget.Add(target);

            int baseAttack = Attack * 5;
            int baseDamages = Damages * 5;

            int damageTaken;

            damageTaken = target.Defend(baseAttack, baseDamages, this);

            // tant que le sort inflige des dégats
            while (damageTaken > 0)
            {
                target = SelectSpellSecondaryTarget();
                if (target != null)
                {
                    double newAttack = (double)baseAttack * (10 - AttackedTarget.Count) / 10.0;
                    double newDammage = (double)baseDamages * (10 - AttackedTarget.Count) / 10.0;
                    damageTaken = target.Defend(Convert.ToInt32(newAttack), Convert.ToInt32(newDammage), this);

                    AttackedTarget.Add(target);
                } else
                {
                    damageTaken = 0;
                }
            }

            base.ActiveSpell();
        }

        private Character SelectSpellPrimaryTarget()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> validTarget = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0 && currentCharacter.IsVisible)
                {
                    //on l'ajoute à la liste des cible valide
                    validTarget.Add(currentCharacter);
                }
            }

            if (validTarget.Count > 0)
            {
                //on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque
                return validTarget[random.Next(0, validTarget.Count)];
            }

            return null;
        }

        private Character SelectSpellSecondaryTarget()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> validTarget = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0 && !AttackedTarget.Contains(currentCharacter))
                {
                    //on l'ajoute à la liste des cible valide
                    validTarget.Add(currentCharacter);
                }
            }

            if (validTarget.Count > 0)
            {
                //on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque
                return validTarget[random.Next(0, validTarget.Count)];
            }

            return null;
        }

        public override Character Clone()
        {
            return new Magicien(Name);
        }
    }
}
