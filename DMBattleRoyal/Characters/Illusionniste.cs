﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Illusionnistse : LivingBeing
    {
        List<Illusionnistse> listClone;
        public Boolean IsClone = false;
        public Illusionnistse parent;

        public Illusionnistse(string name) : base(name, 75, 75, 1, 50, 100, 0.5) {
            listClone = new List<Illusionnistse>();
            // Ajoute l'illusioniste dans la liste pour faciliter la distribution des dégat avec les clones
            listClone.Add(this);
        }

        public override void ActiveSpell()
        {
            // Créé un clone et l'ajoute dans les différentes liste
            Illusionnistse cloneIlu = new Illusionnistse(Name);
            cloneIlu.Reset();
            cloneIlu.IsClone = true;
            cloneIlu.parent = this;
            cloneIlu.SetFightManager(this.fightManager);

            fightManager.charactersList.Add(cloneIlu);
            listClone.Add(cloneIlu);
            // Augmentation des dégats
            Attack += 10;

            base.ActiveSpell();
        }

        public override int Defend(int _attackValue, int _damage, Character _attacker)
        {
            if (CurrentLife < 0)
            {
                return 0;
            }

            if (listClone.Count > 1 && !IsClone)
            {
                // Récupération de la cible
                int a = random.Next(0, listClone.Count);
                Illusionnistse clone = listClone[a];
                
                int damageTaken = clone.RedirectDefense(_attackValue, _damage, _attacker);
                // On tu le clone si il n'est pas mort
                if (damageTaken > 0 && clone != this && clone.CurrentLife > 0)
                {
                    clone.TakeDamages(Int32.MaxValue/2);
                }
                return damageTaken;
            }
            
            // Cas par defaut
            return base.Defend(_attackValue, _damage, _attacker);
        }

        private int RedirectDefense(int _attackValue, int _damage, Character _attacker)
        {
            if (!IsClone)
            {
                //On fait en sorte que le Parent subisse les degats sinon il y a un redirection à l'infini
                //On calcule la marge d'attaque
                //en soustrayant le jet de defense du personnage qui defend au jet d'attaque reçu
                int AttaqueMargin = _attackValue - (Defense + RollDice());
                //Si la marge d'attaque est supérieure à 0
                if (AttaqueMargin > 0)
                {
                    //on calcule les dégâts finaux
                     return TakeDamages((int)(AttaqueMargin * _damage / 100f));
                }
                return 0;
            }
            // Je suis un clone
            return base.Defend(_attackValue, _damage, _attacker);
        }

        public void CloneIsDead(Illusionnistse clone)
        {
            listClone.Remove(clone);
            Attack -= 10;
        }
        public override void ImDead()
        {
            if (IsClone)
            {
                // On informe que le clone est mort
                parent.CloneIsDead(this);
            } else
            {
                
                listClone.Remove(this);

                // on tue tout les clone
                while (listClone.Count > 0)
                {
                    listClone[0].TakeDamages(Int32.MaxValue/2);
                }
            }
            base.ImDead();
        }

        public override Character Clone()
        {
            return new Illusionnistse(Name);
        }
    }
}
