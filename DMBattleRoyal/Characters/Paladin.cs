﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Paladin : LivingBeing
    {
        public Paladin(string name) : base(name, 100, 125, 1.5, 90, 150, 1, true) { }

        public override void ActiveSpell()
        {
            CurrentLife += MaxLife / 10;
            CurrentLife = CurrentLife > MaxLife ? MaxLife : CurrentLife;
            base.ActiveSpell();
        }

        public override void SelectTargetAndAttack()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> validTarget = new List<Character>();
            List<Character> prioTarget = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0 && currentCharacter.IsVisible)
                {
                    //on l'ajoute à la liste des cible valide
                    if (currentCharacter.Cursed)
                    {
                        prioTarget.Add(currentCharacter);
                    } else
                    {
                        validTarget.Add(currentCharacter);
                    }
                }
            }

            if (prioTarget.Count > 0)
            {
                Character target = prioTarget[random.Next(0, prioTarget.Count)];
                MakeAnAttack(target);
            }
            else if (validTarget.Count > 0)
            {
                Character target = validTarget[random.Next(0, validTarget.Count)];
                MakeAnAttack(target);
            }
        }

        public override Character Clone()
        {
            return new Paladin(Name);
        }
    }
}
