﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Vampire : Undead
    {

        private int lastCurrentLife;
        public Vampire(string name) : base(name, 125, 125, 2, 50, 150, 0.2)
        {
            lastCurrentLife = CurrentLife;
        }

        public override void ActiveSpell()
        {
                      
            Character c = SelectSpellTarget();

            if (c != null)
            {
                c.nextAttack.AddMilliseconds(lastCurrentLife - CurrentLife);
                lastCurrentLife = CurrentLife;
                base.ActiveSpell();
            }
        }

        protected override void MakeAnAttack(Character target)
        {
            CurrentLife += target.Defend(Attack + RollDice(), Damages, this);
            CurrentLife = CurrentLife > MaxLife ? MaxLife : CurrentLife;
            AttackCooldown();
        }

        private Character SelectSpellTarget()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> validTarget = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0 && currentCharacter.IsVisible)
                {
                    //on l'ajoute à la liste des cible valide
                    validTarget.Add(currentCharacter);
                }
            }

            if (validTarget.Count > 0)
            {
                //on prend un personngae au hasard dans la liste des cibles valides et on le designe comme la cible de l'attaque
                return validTarget[random.Next(0, validTarget.Count)];
            }

            return null;
        }

        public override Character Clone()
        {
            return new Vampire(Name);
        }
    }
}
