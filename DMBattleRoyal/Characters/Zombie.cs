﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Zombie : Undead
    {

        private HashSet<Character> personneMangee = new HashSet<Character>();
        public Zombie(string name) : base(name, 150, 0, 1.0, 20, 1500, 0.1) { }

        public override int Defend(int _attackValue, int _damage, Character _attacker)
        {
            if (CurrentLife > 0)
            {
                //on calcule les dégâts finaux
                int finalDamages = (int)(_attackValue * _damage / 100f);
                if (_attacker.HolyDamages)
                {
                    finalDamages *= 2;
                }
                return TakeDamages(finalDamages);
            }
            return 0;
        }

        public override void ActiveSpell()
        {
            foreach (Character pers in fightManager.deadCharacter)
            {
                if (!personneMangee.Contains(pers))
                {
                    CurrentLife += pers.getMaxLife();
                    personneMangee.Add(pers);
                }
            }
            base.ActiveSpell();
        }

        public override Character Clone()
        {
            return new Zombie(Name);
        }
    }
}
