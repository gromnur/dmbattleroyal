﻿using DMBattleRoyal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Alchimiste : LivingBeing
    {
        public Alchimiste(string name) : base(name, 50, 50, 1, 30, 150, 0.1, true) { }

        public override void ActiveSpell()
        {
            // Cherche le personnage avec le plus de PV
            int maxLifeValue = fightManager.charactersList.Max(c => c.CurrentLife);
            Character cible = fightManager.charactersList.First(c => c.CurrentLife == maxLifeValue);

            // La cible aura toujours un MaxLife suppérieur au PV de l'alchimiste
            cible.CurrentLife = CurrentLife;
            CurrentLife = maxLifeValue > MaxLife ? MaxLife : maxLifeValue;

            base.ActiveSpell();
        }

        public override void SelectTargetAndAttack()
        {
            //on cree une liste dans laquelle on stockera les cibles valides
            List<Character> targetList = new List<Character>();

            foreach (Character currentCharacter in fightManager.charactersList)
            {
                //si le personnage testé n'est pas celui qui attaque et qu'il est vivant
                if (currentCharacter != this && currentCharacter.CurrentLife > 0)
                {
                    //on l'ajoute à la liste des cible valide
                    if (random.Next(0, 2) == 1)
                    {
                        targetList.Add(currentCharacter);
                    }                
                }
            }

            foreach (Character c in targetList)
            {
                MakeAnAttack(c);
            }
        }

        protected override void MakeAnAttack(Character target)
        {
            // Ajoute les dégat de poison
            if (target is ICanTakePoison)
            {
                int poison = Attack * Damages / 100;
                ((ICanTakePoison)target).AddPoisonStack(poison);
            }
            target.Defend(Attack + RollDice(), Damages/2, this);
            base.AttackCooldown();
        }

        public override int RollDice()
        {
            return random.Next(1, 201);
        }

        public override Character Clone()
        {
            return new Alchimiste(Name);
        }
    }
}
