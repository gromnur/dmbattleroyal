﻿using DMBattleRoyal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Assassin : LivingBeing
    {
        public Assassin(string name) : base(name, 150, 100, 1, 100, 185, 0.5) {
           
        }

        public override void ActiveSpell()
        {
            if ((fightManager.charactersList.Count - fightManager.deadCharacter.Count) >= 5)
            {
                IsVisible = false;
            }
            base.ActiveSpell();
        }

        protected override void MakeAnAttack(Character target)
        {
            // Poison dammage
            if (target is ICanTakePoison)
            {
                ((ICanTakePoison)target).AddPoisonStack(Attack * Damages / 1000);
            }

            // PV de la cible avant l'attack
            int targetHP = target.CurrentLife;

            int dammageDone = target.Defend(Attack + RollDice(), Damages, this);

            if (dammageDone > targetHP/2 && target.CurrentLife > 0)
            {
                target.TakeDamages(Int32.MaxValue / 2);
            }
            AttackCooldown();
        }

        public override int TakeDamages(int _damages)
        {
            IsVisible = true;
            return base.TakeDamages(_damages);
        }

        public override Character Clone()
        {
            return new Assassin(Name);
        }
    }
}
