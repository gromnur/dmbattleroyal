﻿using DMBattleRoyal.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Necromencien : Undead
    {

        public Necromencien(string name) : base(name, 0, 10, 1.0, 0, 275, 5) { }

        public override void ImDead()
        {
            fightManager.SomeOneIsDead -= SomeOneIsDead;
            base.ImDead();
        }
        public override void ActiveSpell()
        {
            if ((fightManager.charactersList.Count - fightManager.deadCharacter.Count) >= 5)
            {
                IsVisible = false;
            }
            base.ActiveSpell();
        }

        protected override void MakeAnAttack(Character target)
        {
            IsVisible = true;
            // Poison dammage
            if (target is ICanTakePoison)
            {
                ((ICanTakePoison)target).AddPoisonStack(Attack * Damages / 500);
            }

            target.Defend(Attack + RollDice(), Damages/2, this);

            AttackCooldown();
        }

        public override int RollDice()
        {
            return random.Next(1, 151);
        }

        public void SomeOneIsDead(Character c)
        {
            Attack += 5;
            Defense += 5;
            Damages += 5;
            MaxLife += 50;
            CurrentLife += 50;
        }

        public override int TakeDamages(int _damages)
        {
            IsVisible = true;
            return base.TakeDamages(_damages);
        }

        public override Character Clone()
        {
            return new Necromencien(Name);
        }

        public override void SetFightManager(FightManager fightManager)
        {
            base.SetFightManager(fightManager);
            fightManager.SomeOneIsDead += SomeOneIsDead;
        }
    }
}
