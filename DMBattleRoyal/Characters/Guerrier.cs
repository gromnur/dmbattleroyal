﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DMBattleRoyal
{
    class Guerrier : LivingBeing
    {
        public Guerrier(string name) : base(name, 150, 105, 2.2, 150, 250, 0.2) {
           
        }

        public override void ActiveSpell()
        {
            base.ActiveSpell();
            // gestion du pouvoir
            AttackSpeed += 3.0;
            Task.Run(() => SpellDuration());
        }

        public void SpellDuration()
        {
            Thread.Sleep(3000);
            AttackSpeed -= 3.0;
        }

        public override Character Clone()
        {
            return new Guerrier(Name);
        }
    }
}
